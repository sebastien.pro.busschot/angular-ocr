import { Component, OnInit, OnDestroy } from '@angular/core';
import { interval, Subscription} from 'rxjs';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy{
  
  secondes: number;
  counterSubscription: Subscription;
  isAuth: boolean;

  constructor( private authService: AuthService) {
    this.isAuth = this.authService.isAuth;
    console.log(this.isAuth);
  }
  
  ngOnInit() {
    
    const counter = interval(1000);
    counter.subscribe(
      (value) => {
        this.secondes = value;
      },
      (error) => {
        console.log('Uh-oh, an error occurred! : ' + error);
      },
      () => {
        console.log('Observable complete!');
      }
    );
  }

  ngOnDestroy() {
    this.counterSubscription.unsubscribe();
  }

}